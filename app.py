from flask import Flask, render_template, url_for, flash, request, session, redirect
import pymongo
import json

import lstm

app = Flask(__name__)

app.secret_key = "akljshfdu124@1234qh92h3riu12fn1ihKJAN*(Q!@R"

client=pymongo.MongoClient('mongodb://Mounika21:21Mounika@cluster0-shard-00-00-x18ac.mongodb.net:27017,cluster0-shard-00-01-x18ac.mongodb.net:27017,cluster0-shard-00-02-x18ac.mongodb.net:27017/SIH?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true')
db=client.SIH	

def get_phcs():
	phcs = db.PHC.find()
	a = list(phcs)
	phcs = {'name': list(a[0]['phc name'].values()), 
	'street address': list(a[0]['street address'].values()),
	'district': list(a[0]['district '].values()),
	'pincode': list(a[0]['pin code'].values()),
	'state': list(a[0]['state'].values()),
	'latitude': list(a[0]['Latitude'].values()),
	'longitude': list(a[0]['Longitude'].values())}
	for i in range(1,len(a)):
		phcs['name'].append(a[i]['phc name'])
		phcs['street address'].append(a[i]['street address'])
		phcs['district'].append(a[i]['district'])
		phcs['pincode'].append(a[i]['pincode'])
		phcs['state'].append(a[i]['phc state'])
		phcs['latitude'].append(a[i]['latitude'])
		phcs['longitude'].append(a[i]['longitude'])
	return phcs
		


def validate_login(uname, pwd):	
	collection=db.AuguStrides
	d = collection.find({"username": uname})
	a = []
	for i in d:
		a.append(i)

	if len(a) != 0 and uname == "officer1":
		return 1
	elif len(a) != 0 and uname == "incharge1":
		return 2
	else:
	 	return -1
   


@app.route('/')
def hello_world():
   return render_template("login.html")

@app.route('/login',methods = ['POST', 'GET'])
def login_verify():
	if request.method == 'POST':
		result = request.form
		n = validate_login(result["uname"], result["pwd"])
		if n == 1:
			session["uname"] = "officer"
			phcs = get_phcs()
			print(phcs)
			return render_template("index.html", phcs = phcs)
		elif n == 2:
			session["uname"] = "incharge"
			phcs = get_phcs()
			print(phcs)
			return render_template("index.html", phcs = phcs)
		else:
			flash("Enter valid user credentials!")
			return redirect(url_for("hello_world"))

@app.route('/phc/<int:num_phc>')
def phc_report(num_phc):
	phcs = get_phcs()
	return render_template("report.html", name = phcs["name"][num_phc], 
										address = phcs['street address'][num_phc],
										state = phcs['state'][num_phc],
										district = phcs["district"][num_phc])

@app.route("/past10")
def past10():
	return render_template("pie.html")

@app.route("/meds")
def medss():
	return render_template("pastmedicine.html")

@app.route("/docs1")
def docs():
	return render_template("pastdoctors.html")

@app.route("/docs2")
def docs2():
	data = lstm.main()
	print(data)
	print(len(data))
	return render_template("futuredocs.html", data = data)

@app.route('/logout')
def logout():
	session.pop('uname', None)
	return redirect(url_for("hello_world"))

@app.route('/addphc')
def addphc():
	return render_template('index2.html')

@app.route('/registerphc', methods = ["POST", "GET"])
def regphc():
	if request.method == "POST":
		result = request.form
		d = {"phc name": result["name"], 
		"street address": result["place"],
		"district": "",
		"pincode": "",
		"phc state": "",
		"latitude": float(result["lat"]),
		"longitude": float(result["long"])}
		db.PHC.insert_one(d)
	return render_template('index.html')
if __name__ == '__main__':
   app.run()